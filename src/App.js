import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider} from 'mobx-react';
import data from './store/store';
import Button from 'material-ui/Button';

import LocationInput from './components/LocationInput';
import TechnologyInput from './components/TechnologyInput';
import {inject, observer} from 'mobx-react';



class App extends Component {


  render() {
    return (
        <Provider data={data}>
              <div className="App">
                <LocationInput/>
                <TechnologyInput/>
              </div>
        </Provider>
    );
  }
}

export default App;

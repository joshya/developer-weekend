import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import Button from 'material-ui/Button'










@inject('data')
@observer
class TechnologyInput extends Component{





    render(){

        const data = this.props.data.listOfTechnology;

        const items = data.map((item) => {
            return <Button onClick={()=>{this.props.data.onTechnologyButtonClick(item)}} key={item}>{item}</Button>
        } );


        return(
            <div>
                <p>{items}</p>
            </div>
        )


    }
}



export default TechnologyInput


